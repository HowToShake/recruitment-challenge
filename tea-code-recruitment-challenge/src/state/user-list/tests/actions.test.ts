import {getUsers, UserListActionTypes} from "../actions";
import configureStore from 'redux-mock-store';
import thunk, { ThunkDispatch } from 'redux-thunk';
import * as axios from 'axios'
import {AnyAction} from "redux";

const initialState = {
    users: {}
};
type State = typeof initialState;
const middlewares = [thunk];
const mockStore = configureStore<State, ThunkDispatch<State, any, AnyAction>>(middlewares);
const store = mockStore(initialState);

jest.mock("axios");

describe('When testing user-list actions', () => {
    const endpointUrl = 'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json'
    afterEach(() => {
        store.clearActions();
    });
    describe('getUsers', () => {
        test('Should fetch users successfully and dispatch ADD_USERS action', async () => {
            const dataFromApi = [{id: '1'}, {id: '2'}, {id: '3'}];
            // @ts-ignore
            axios.get.mockImplementation(() => Promise.resolve({status: 200, data: dataFromApi}));

            await store.dispatch(getUsers);

            // @ts-ignore
            expect(axios.get).toBeCalledTimes(1);
            // @ts-ignore
            expect(axios.get).toBeCalledWith(endpointUrl);
            // @ts-ignore
            expect(store.getActions()).toEqual([
                { type: UserListActionTypes.ADD_USERS, payload: dataFromApi },
            ])
        })

        test('Should fetch users unsuccessfully and console.log error', async () => {
            // @ts-ignore
            axios.get.mockImplementation(() => Promise.reject(new Error('Custom')));
            console.log = jest.fn();

            await store.dispatch(getUsers);

            // @ts-ignore
            expect(axios.get).toBeCalledTimes(1);
            // @ts-ignore
            expect(store.getActions()).toEqual([])
            // @ts-ignore
            expect(console.log).toHaveBeenCalledWith(Error('Custom'))
        })
    })
})

