import {userListReducer as reducer} from '../reducers'
import {UserListActionTypes} from '../actions'
import thunk, {ThunkDispatch} from "redux-thunk";
import configureStore from "redux-mock-store";
import {AnyAction} from "redux";

const initialState = {};
type State = typeof initialState;
const middlewares = [thunk];
const mockStore = configureStore<State, ThunkDispatch<State, any, AnyAction>>(middlewares);
const store = mockStore(initialState);

describe('When testing user-list reducers', () => {
    const payload = [
        {
            id: 1,
            last_name: 'B',
            first_name: 'B',
            email: 'B',
            gender: 'Female',
            avatar: 'avatar'
        },
        {
            id: 2,
            last_name: 'A',
            first_name: 'A',
            email: 'A',
            gender: 'Female',
            avatar: 'avatar'
        },
        {
            id: 3,
            last_name: 'C',
            first_name: 'C',
            email: 'C',
            gender: 'Female',
            avatar: 'avatar'
        },
        {
            id: 3,
            last_name: 'D',
            first_name: 'D',
            email: 'D',
            gender: 'Female',
            avatar: 'avatar'
        },
    ]
    afterEach(() => {
        store.clearActions();
    });

    describe('DEFAULT', () => {
        test('Should return initial state', () => {
            expect(reducer(undefined, {type: 'Custom'})).toEqual({
                users: [],
                selectedUserIds: []
            })
        })
    })

    describe('ADD_USERS', () => {
        const initialState = {
            users: [],
            selectedUserIds: []
        }
        test('Should return uniq ordered list by last_name', () => {
            expect(reducer(initialState, {type: UserListActionTypes.ADD_USERS, payload})).toEqual({
                users: [
                    {
                        id: 2,
                        last_name: 'A',
                        first_name: 'A',
                        email: 'A',
                        gender: 'Female',
                        avatar: 'avatar'
                    },
                    {
                        id: 1,
                        last_name: 'B',
                        first_name: 'B',
                        email: 'B',
                        gender: 'Female',
                        avatar: 'avatar'
                    },
                    {
                        id: 3,
                        last_name: 'C',
                        first_name: 'C',
                        email: 'C',
                        gender: 'Female',
                        avatar: 'avatar'
                    }
                ],
                selectedUserIds: []
            })
        })

        test('Should return initial state because of undefined payload', () => {
            expect(reducer(initialState, {type: UserListActionTypes.ADD_USERS, payload: undefined})).toEqual({
                users: [],
                selectedUserIds: []
            })
        })
    })

    describe('TOGGLE_SELECTED_USER_ID', () => {
        const initialState = {
            users: [],
            selectedUserIds: []
        }
        test('Should add selected user Id', () => {
            expect(reducer(initialState, {type: UserListActionTypes.TOGGLE_SELECTED_USER_ID, payload: payload[0].id})).toEqual({
                users: [],
                selectedUserIds: [1]
            })
        })

        test('Should remove user Id if exists in state', () => {
            expect(reducer({...initialState, selectedUserIds: [1]}, {type: UserListActionTypes.TOGGLE_SELECTED_USER_ID, payload: payload[0].id})).toEqual({
                users: [],
                selectedUserIds: []
            })
        })

        test('Should return default state when payload is undefined', () => {
            expect(reducer(initialState, {type: UserListActionTypes.TOGGLE_SELECTED_USER_ID, payload: undefined})).toEqual({
                users: [],
                selectedUserIds: []
            })
        })
    })
})
