import Urls from "../../constants";
import axios from "axios";
import { ThunkDispatch } from "redux-thunk";
import { UserModel } from "./reducers";

export enum UserListActionTypes {
    ADD_USERS = 'ADD_USERS',
    TOGGLE_SELECTED_USER_ID = 'TOGGLE_SELECTED_USER_ID',
}

export const getUsers = async (dispatch: ThunkDispatch<any, void, any>) => {
    try {
        const {status, data} = await axios.get(`${process.env.REACT_APP_ENDPOINT}${Urls.users}`);

        if(status === 200){
            dispatch(addUsers(data))
        }
    }catch (e) {
        console.log(e);
    }
}

const addUsers = (users: UserModel[]) => ({
    type: UserListActionTypes.ADD_USERS,
    payload: users
})

export const toggleSelectedUserId = (userId: number) => ({
    type: UserListActionTypes.TOGGLE_SELECTED_USER_ID,
    payload: userId
})
