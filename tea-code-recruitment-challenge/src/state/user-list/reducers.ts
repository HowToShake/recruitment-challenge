import { UserListActionTypes } from "./actions";
import _ from 'lodash'

const initialUserListState = {
    users: [],
    selectedUserIds: []
}

export const userListReducer = (state: UserListState = initialUserListState, action: ActionState) => {
    switch (action.type){
        case UserListActionTypes.ADD_USERS: {
            if(action.payload) {
                return {
                    ...state,
                    users: _.orderBy(_.uniqBy([...state.users, ...action.payload], 'id'), (user) => user.last_name)
                }
            }
            return state;
        }
        case UserListActionTypes.TOGGLE_SELECTED_USER_ID: {
            const isUserIdInState = state.selectedUserIds?.find((userId: number) => userId === action.payload);
            if(action.payload) {
                if (isUserIdInState) {
                    return {
                        ...state,
                        selectedUserIds: _.filter(state.selectedUserIds, (userId) => userId !== action.payload)
                    }
                } else {
                    return {
                        ...state,
                        selectedUserIds: _.uniq([...state.selectedUserIds, action.payload])
                    }
                }
            }
            return state;
        }
        default:
            return state;
    }
}

interface ActionState{
    type: string;
    payload?: any;
}

interface UserListState {
    users: UserModel[];
    selectedUserIds: number[];
}

export type UserModel = {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    gender: "Male" | "Female";
    avatar: string;
}