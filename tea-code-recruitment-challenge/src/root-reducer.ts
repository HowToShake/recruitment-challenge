import { combineReducers } from 'redux'
import { userListReducer } from './state/user-list/reducers'

export default combineReducers({
    users: userListReducer,
});