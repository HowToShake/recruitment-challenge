import {memo} from "react";
import UserListView from "./components/user-list/user-list-view";
import 'antd/dist/antd.css';
import {Layout} from "antd";

const { Header, Content } = Layout;


const App = () => {
  return (
      <>
        <Header style={{backgroundColor: '#FFFFFF', textAlign: 'center' }}>
          TeaCode Recruitment Challenge made by Michał Ryśkiewicz
        </Header>
        <Content>
          <UserListView />
        </Content>
      </>
  );
}

export default memo(App);
