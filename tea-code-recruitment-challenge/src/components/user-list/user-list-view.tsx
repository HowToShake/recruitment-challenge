import {List, Avatar, Checkbox, Input} from "antd";
import {ChangeEvent, memo, useEffect, useState} from "react";
import { UserModel } from "../../state/user-list/reducers";
import {useDispatch, useSelector} from "react-redux";
import {getUsers, toggleSelectedUserId} from "../../state/user-list/actions";
import {selectedUserIds, selectUsers} from "../../state/user-list/selectors";

const { Search } = Input;

const UserList = () => {
    const dispatch = useDispatch();
    const users = useSelector(selectUsers);
    const selectedUsers = useSelector(selectedUserIds);
    const [searchedPhrase, setSearchedPhrase]: any = useState('');

    useEffect(() => {
        dispatch(getUsers);
    }, [])

    useEffect(() => {
        console.log('SelectedUsers', selectedUsers);
    }, [selectedUsers])

    const filterUsers = () => {
        return users?.filter((user: UserModel) => {
            if (user.first_name.includes(searchedPhrase) || user.last_name.includes(searchedPhrase)) {
                return user
            }
        })
    }

    return(
        <>
            <Search placeholder="Find by first name or last name " onChange={(e: ChangeEvent<HTMLInputElement>) => setSearchedPhrase(e.target.value)} />
            <List
                bordered
                itemLayout="horizontal"
                dataSource={ searchedPhrase ? filterUsers() : users}
                pagination={{
                    total: searchedPhrase ? filterUsers()?.length : users?.length,
                    defaultPageSize: 10,
                }}
                renderItem={({id, avatar, first_name, last_name}: UserModel) => (
                    <List.Item>
                        <List.Item.Meta
                            avatar={<Avatar src={avatar} />}
                            title={`${first_name}${' '}${last_name}`}
                        />
                        <Checkbox checked={selectedUsers?.find((userId: number) => userId === id)} onClick={() => dispatch(toggleSelectedUserId(id))}/>
                    </List.Item>
                )}
                />
        </>
    )
}

export default memo(UserList);

